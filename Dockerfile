# Base image to ubuntu:16.04
FROM ubuntu:16.04

# Author WHO
MAINTAINER jimmy<jimmy92049@gmail.com>

RUN apt-get -y update && \
apt-get -y upgrade && \
apt-get	install	-y apache2

CMD /bin/bash \
chmod 755 /var/www/html 


# put my local web site in testweb2 folder to /var/www/html
ADD testweb2 /var/www/html

# expose httpd port
EXPOSE 80


